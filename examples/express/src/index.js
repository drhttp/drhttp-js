
const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const _ = require('lodash')
    ;
app.use(bodyParser.json());
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; // disable https certificate verification

// Auth
app.use(require('express-basic-auth')({
    authorizer: function (user, password) {
        return true // allow any user/password
    }
}))

// Moesif

process.env['DRHTTP_DSN'] = "https://481372d9-6e4c-49d4-aa31-08465c78b218@api.drhttp.172-16-43-9.traefik.me/"

const drhttp_dsn = new URL(process.env['DRHTTP_DSN'])
app.use(require('moesif-nodejs')({
    applicationId: _.padEnd(drhttp_dsn.username, 50, '-'),
    baseUri: drhttp_dsn.protocol + '//' + drhttp_dsn.host + '/moesif',
    // debug: true,
    identifyUser: function (req, res) {
        return (req.auth || {}).user;
    },
}));

// Routes

app.all('/', function (req, res) {
    const user = (req.auth || {}).user || "world"
    res.send('Hello ' + user + '!<br>This is an example. It has the role of your server of which you want to record io requests.')
})

app.all('/crash_test', function (req, res) {
    res.send(1 / 0)
})

app.all('/unexisting_path', function (req, res) {
    res.status(404).send('Not found')
})

app.all('/*', function (req, res) {
    res.json({
        "user": (req.auth || {}),
        "path": req.path,
        "echo data": req.body
    });
});

// Run
app.listen(8012, () => console.log(`Example app listening on port 80!`))


