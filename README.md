This is the official [NodeJS](https://nodejs.org/) (and [ExpressJS](https://expressjs.com/)) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [nodejs library](https://github.com/Moesif/moesif-express). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/moesif-express/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation

 1) Install package with `npm` (or any compatible package manager) :
    ```
    npm install --save moesif-express
    ```

 2) You will need a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects).

# Usage for [ExpressJS](https://expressjs.com/)

[An integration example is provided here](https://bitbucket.org/drhttp/drhttp-js/src/master/examples/express/)

## Inbound request recording

```javascript

...
const app = express()
...
const drhttp_dsn = new URL('<insert_dsn_here>')
app.use(require('moesif-express')({
  applicationId: drhttp_dsn.username,
  baseUri: drhttp_dsn.protocol + '//' + drhttp_dsn.host + '/moesif',
}));
...

```

### User identification

See [identifiUser in Moesif documentation](https://github.com/Moesif/moesif-express#identifyuser)

### Device identification

*Note: Device identification is not available yet in the nodejs library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the nodejs library*

# Usage for [NodeJS](https://nodejs.org/)

## Inbound request recording

```javascript
const http = require('http');
var moesifExpress = require('moesif-express');

const drhttp_dsn = new URL('<insert_dsn_here>')
var options = {
  applicationId: drhttp_dsn.username,
  baseUri: drhttp_dsn.protocol + '//' + drhttp_dsn.host + '/moesif',
};

var server = http.createServer(function (req, res) {
  moesifExpress(options)(req, res, function () {});

  // Your code...
  req.on('end', function () {
    res.write(JSON.stringify({
      message: "hello world!",
      id: 2
    }));
    res.end();
  });
});

server.listen(8080);
```

### User identification

See [identifiUser in Moesif documentation](https://github.com/Moesif/moesif-express#identifyuser)

### Device identification

*Note: Device identification is not available yet in the nodejs library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the nodejs library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-js/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
